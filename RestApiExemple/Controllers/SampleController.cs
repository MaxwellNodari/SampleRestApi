using FrameworkExemple.Models;
using FrameworkExemple.Services;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace RestApiExemple.Controllers
{
    [ApiController]
    [Route("api/samples")]
    public class SampleController : Basic
    {
        
        private readonly ILogger<SampleController> _logger;
        private readonly ISampleService _sampleService;

        public SampleController(ILogger<SampleController> logger, ISampleService sampleService = null)
        {
            _logger = logger;
            _sampleService = sampleService;
        }

        [HttpGet("{idSample}")]
        [ProducesResponseType(typeof(SampleModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public async Task<IActionResult> Get([FromRoute] int idSample)
        {
            try
            {
                SampleModel response = await _sampleService.GetByIdAsync(idSample);

                if (response is null)
                    return NotFound();

                return Ok(response);
            }
            catch (Exception e)
            {
                AddLog(_logger, LogLevel.Error, e);
                return BuildException(e);
            }
        }

        [HttpGet("")]
        [ProducesResponseType(typeof(List<SampleModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public async Task<IActionResult> Listing()
        {
            try
            {
                List<SampleModel> response = await _sampleService.ListingSamplesAsync();

                if (response?.Count == 0)
                    return NotFound();

                return Ok(response);
            }
            catch (Exception e)
            {
                AddLog(_logger, LogLevel.Error, e);
                return BuildException(e);
            }
        }

        
    }
}