﻿using FrameworkExemple.Models;
using FrameworkExemple.Services;
using Microsoft.AspNetCore.Mvc;

namespace RestApiExemple.Controllers
{
    [Route("api/zipCodes")]
    [ApiController]
    public class ZipCodeController : Basic
    {
        private readonly IZipCodeService _zipCodeService;
        private readonly ILogger<ZipCodeController> _logger;
        public ZipCodeController(IZipCodeService zipCodeService, ILogger<ZipCodeController> logger = null)
        {
            _zipCodeService = zipCodeService;
            _logger = logger;
        }
        /// <summary>
        /// Get Address by zip code
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        [HttpGet("{zipCode}")]
        [ProducesResponseType(typeof(AddressModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        public async Task<IActionResult> Get([FromRoute] string zipCode)
        {
            try
            {
                AddressModel response = await _zipCodeService.GetAddressByZipCodeAsync(zipCode);

                if (response is null)
                    return NotFound();

                return Ok(response);
            }
            catch (Exception e)
            {
                AddLog(_logger, LogLevel.Error, e);
                return BuildException(e);
            }
        }
    }
}
