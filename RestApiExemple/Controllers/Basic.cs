﻿using AutoMapper.Configuration.Conventions;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace RestApiExemple.Controllers
{
    public class Basic : ControllerBase
    {
        internal IActionResult BuildException(Exception e)
        {
            return BadRequest(new { Error = e.Message });
        }
        internal void AddLog(ILogger _logger, LogLevel level, Exception e = null)
        {
            AddLog(_logger, level, null, e);
        }
        internal void AddLog(ILogger _logger, LogLevel level)
        {
            AddLog(_logger, level);
        }
        internal void AddLog(ILogger _logger, LogLevel level, string? message = null)
        {
            AddLog(_logger, level, message);
        }
        private void AddLog(ILogger _logger, LogLevel level, string? message = null, Exception e = null)
        {
            switch (level)
            {
                case LogLevel.Warning:
                    _logger?.LogWarning($"Processed request {Request.GetDisplayUrl}. {message ?? string.Empty}");
                    break;
                case LogLevel.Error:
                    _logger?.LogError($"An error occurred on request {Request.GetDisplayUrl}. \nError: {e?.Message} \nTrace: {e?.StackTrace}");
                    break;
                default:
                    _logger?.LogInformation($"Processed request {Request.GetDisplayUrl}. {message ?? string.Empty}");
                    break;
            }

        }
    }
}
