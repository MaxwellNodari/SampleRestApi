﻿namespace FrameworkExemple
{
    public class EnvironmentVariables
    {
        public static class ExternalZipCode
        {
            public static string UrlBase = GetVariableValueOrDefault("External.ZipCode", "https://brasilapi.com.br");
        }
        private static string GetVariableValueOrDefault(string variableName, string defaultValue = "")
        {
            string value = Environment.GetEnvironmentVariable(variableName, EnvironmentVariableTarget.Process);
            return value ?? defaultValue;
        }
    }
}