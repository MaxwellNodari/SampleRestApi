﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace FrameworkExemple.AutoMapper
{
    public static class AutoMapperDependency
    {
        public static void AutoMapperConfiguration(this IServiceCollection services)
        {
            var _config = new MapperConfiguration(cfg =>
            {
                AutoMapperProfile.DefaultMappers(cfg);
            });

            services.AddScoped(typeof(IMapper), X => _config.CreateMapper());
        }
    }
}
