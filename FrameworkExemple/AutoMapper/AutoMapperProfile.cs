﻿using AutoMapper;
using FrameworkExemple.Entities;
using FrameworkExemple.Models;

namespace FrameworkExemple.AutoMapper
{
    public class AutoMapperProfile : Profile
    {

        public static void DefaultMappers(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<SampleEntity, SampleModel>()
                .ForMember(o => o.Options, opt => opt.MapFrom(x => x.Options))
                .ReverseMap();
            cfg.CreateMap<OptionEntity, OptionModel>()
                 .ReverseMap();
        }
    }
}
