﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrameworkExemple.Entities
{
    public class SampleEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SampleId { get; set; }

        public string Name { get; set; }

        public double? Value { get; set; }

        public ICollection<OptionEntity> Options { get; set; }
    }
}
