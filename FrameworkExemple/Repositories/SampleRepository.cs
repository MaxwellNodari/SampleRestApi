﻿using FrameworkExemple.Entities;

namespace FrameworkExemple.Repositories
{
    public class SampleRepository : ISampleRepository
    {
        public Task<SampleEntity> GetAsync(int idSample)
               => Task.FromResult(BuildSampleExemple(idSample));

        public Task<List<SampleEntity>> ListingAsync()
        {
            List<SampleEntity> sampleEntities = new()
            {
                BuildSampleExemple(1)
            };
            return Task.FromResult(sampleEntities);
        }

        private static SampleEntity BuildSampleExemple(int idSample)
        {
            return new SampleEntity() { SampleId = idSample, Name = "SampleName", Value = 2, Options = BuildOptionListExemple() };
        }

        private static List<OptionEntity> BuildOptionListExemple()
        {
            List<OptionEntity> options = new();
            OptionEntity option = new() { Description = "Description exemple", Name = "Name Exemple", OptionId = 1 };
            options.Add(option);
            return options;
        }
    }
}
