﻿using FrameworkExemple.Entities;

namespace FrameworkExemple.Repositories
{
    public interface ISampleRepository
    {
        Task<SampleEntity> GetAsync(int idSample);
        Task<List<SampleEntity>> ListingAsync();
    }
}
