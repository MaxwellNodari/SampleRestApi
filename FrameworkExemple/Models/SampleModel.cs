﻿using Newtonsoft.Json;

namespace FrameworkExemple.Models
{
    public class SampleModel
    {
        [JsonProperty("sampleId", NullValueHandling = NullValueHandling.Ignore)]
        public int SampleId { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public double? Value { get; set; }

        [JsonProperty("options", NullValueHandling = NullValueHandling.Ignore)]
        public List<OptionModel> Options { get; set; }
    }
}
