﻿using Newtonsoft.Json;

namespace FrameworkExemple.Models
{
    public class OptionModel
    {
        [JsonProperty("optionId", NullValueHandling = NullValueHandling.Ignore)]
        public long OptionId { get; set; }
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
    }
}
