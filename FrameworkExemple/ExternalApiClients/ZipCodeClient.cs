﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FrameworkExemple.Models;
using RestSharp;
using FrameworkExemple.ExternalApiClients.Generics;

namespace FrameworkExemple.ExternalApiClients
{
    public class ZipCodeClient : IZipCodeClient
    {
        private readonly string _urlBase;
        public ZipCodeClient()
        {
            _urlBase = EnvironmentVariables.ExternalZipCode.UrlBase;
        }

        public async Task<AddressModel> GetAddressByZipCodeAsync(string zipCode)
        {
            GenericClient client = new(_urlBase);
            try
            {
                var resource = $"/api/cep/v2/{zipCode}";

                RestResponse response = await client.GetAsync(resource);

                return JsonConvert.DeserializeObject<AddressModel>(response.Content);

            }
            catch (Exception e)
            {
                throw new Exception($"Erro ao buscar o Cep {zipCode}. Erro: {e.Message}");
            }
        }

    }
}
