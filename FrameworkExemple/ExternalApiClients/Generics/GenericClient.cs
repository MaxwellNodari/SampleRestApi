﻿using RestSharp;

namespace FrameworkExemple.ExternalApiClients.Generics
{
    public class GenericClient
    {
        private readonly string _urlBase;

        public GenericClient(string urlBase)
        {
            _urlBase = urlBase;
        }
        public async Task<RestResponse> GetAsync(string resource)
        {
            return await ExecuteAsync(resource, Method.Get);
        }
        private async Task<RestResponse> ExecuteAsync(string resource, Method method, string jsonBody = "")
        {
            var request = new RestRequest(resource, method);
            request.Timeout = -1;

            using var client = new RestClient(_urlBase);

            if(method!= Method.Get && !string.IsNullOrEmpty(jsonBody))
            {
                request.AddJsonBody(jsonBody);
            }

            RestResponse response = await client.ExecuteAsync(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var error = $"Response of {response.ResponseUri}:\n[StatusCode: {response.StatusCode} \nContent: {response.Content}\nDetails: {response.ErrorMessage} - {response.ErrorException}  ]";

                throw new Exception(error);
            }

            return response;
        }
    }
}
