﻿using FrameworkExemple.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkExemple.ExternalApiClients
{
    public interface IZipCodeClient
    {
        Task<AddressModel> GetAddressByZipCodeAsync(string zipCode);
    }
}
