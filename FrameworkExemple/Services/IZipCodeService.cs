﻿using FrameworkExemple.Models;

namespace FrameworkExemple.Services
{
    public interface IZipCodeService
    {
        Task<AddressModel> GetAddressByZipCodeAsync(string zipCode);
    }
}
