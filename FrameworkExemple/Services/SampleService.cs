﻿using AutoMapper;
using FrameworkExemple.Entities;
using FrameworkExemple.Models;
using FrameworkExemple.Repositories;
using System.Collections.Generic;

namespace FrameworkExemple.Services
{
    public class SampleService : ISampleService
    {
        private readonly ISampleRepository _sampleRepository;
        private readonly IMapper _mapper;

        public SampleService(IMapper mapper, ISampleRepository sampleRepository = null)
        {
            _mapper = mapper;
            _sampleRepository = sampleRepository;
        }

        public async Task<SampleModel> GetByIdAsync(int idSample)
        {
            SampleModel sampleModel = new();

            var sampleEntity = await _sampleRepository.GetAsync(idSample);

            sampleModel = _mapper.Map<SampleEntity, SampleModel>(sampleEntity);

            return sampleModel;
        }

        public async Task<List<SampleModel>> ListingSamplesAsync()
        {
            List<SampleModel> samples = new();

            List<SampleEntity> samplesEntity  = await _sampleRepository.ListingAsync();

            samples = _mapper.Map<List<SampleEntity>, List<SampleModel>>(samplesEntity);

            return samples;
        }
    }
}
