﻿using FrameworkExemple.ExternalApiClients;
using FrameworkExemple.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkExemple.Services
{
    public class ZipCodeService : IZipCodeService
    {
        private readonly IZipCodeClient _zipCodeClient;

        public ZipCodeService(IZipCodeClient zipCodeClient)
        {
            _zipCodeClient = zipCodeClient;
        }

        public async Task<AddressModel> GetAddressByZipCodeAsync(string zipCode)
        {
            var postalCode = new String(zipCode.Where(Char.IsDigit).ToArray());
            AddressModel address =  await _zipCodeClient.GetAddressByZipCodeAsync(zipCode);
            return address;

        }
    }
}
