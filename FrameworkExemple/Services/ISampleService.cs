﻿using FrameworkExemple.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkExemple.Services
{
    public interface ISampleService
    {
        Task<SampleModel> GetByIdAsync(int idSample);
        Task<List<SampleModel>> ListingSamplesAsync();
    }
}
